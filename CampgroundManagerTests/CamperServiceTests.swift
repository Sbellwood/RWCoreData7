//
//  CamperServiceTests.swift
//  CampgroundManagerTests
//
//  Created by Skyler Svendsen on 6/8/18.
//  Copyright © 2018 Razeware. All rights reserved.
//

import CampgroundManager
import CoreData
import XCTest

class CamperServiceTests: XCTestCase {
  
  var camperService: CamperService!
  var coreDataStack: CoreDataStack!

  override func setUp() {
    super.setUp()
    
    coreDataStack = TestCoreDataStack()
    camperService = CamperService(managedObjectContext: coreDataStack.mainContext, coreDataStack: coreDataStack)
    
  }

  override func tearDown() {
    super.tearDown()
    
    camperService = nil
    coreDataStack = nil
    
  }

  func testExample() {
      // This is an example of a functional test case.
      // Use XCTAssert and related functions to verify your tests produce the correct results.
  }

  func testPerformanceExample() {
      // This is an example of a performance test case.
      self.measure {
          // Put the code you want to measure the time of here.
      }
  }
  
  func testAddCamper() {
    let camper = camperService.addCamper("Facon lover", phoneNumber: "(910) 543-9837")
    
    XCTAssertNotNil(camper, "camper should not be nil")
    XCTAssertTrue(camper?.fullName == "Facon lover")
    XCTAssertTrue(camper?.phoneNumber == "(910) 543-9837")
  }
  
  func testRootContextIsSavedAfterAddingCamper() {
    
    //1
    let derivedContext = coreDataStack.newDerivedContext()
    camperService = CamperService(managedObjectContext: derivedContext, coreDataStack: coreDataStack)
    
    //2
    expectation(forNotification: .NSManagedObjectContextDidSave, object: coreDataStack.mainContext) {
      notification in
      return true
    }
    
    //3
    derivedContext.perform {
      let camper = self.camperService.addCamper("Johanna Glrup", phoneNumber: "000-000-0000")
      XCTAssertNotNil(camper)
    }
    
    waitForExpectations(timeout: 2.0) {
      error in
      XCTAssertNil(error, "Save did not occur")
    }
  }

}
